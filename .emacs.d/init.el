
(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize)
  (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
;;  (add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/") t)
  )
(require 'cl)
(defvar xianyyu/packages '(

			   counsel
			   swiper
                           ggtags
                           meghanada
                           company
                           company-irony
                           company-irony-c-headers
                           markdown-mode
                           json-mode
                           yasnippet
                           monokai-theme
                           flycheck
                           haskell-mode
                           jedi
                           jedi-core
			   magit
			   magit-filenotify
			   emms
			   projectile
                           google-c-style
                           flatui-dark-theme
                           darktooth-theme

                           qml-mode
			   popwin
			   emms-player-mpv
			   lua-mode
                           
                           )  "Default packages")

(setq package-selected-packges xianyyu/packages)

(defun  xianyyu/packages-installed-p ()
  (loop for pkg in xianyyu/packages
	when (not (package-installed-p pkg)) do (return nil)
	finally (return t)))

(unless (xianyyu/packages-installed-p)
  (message "%s" "Refreshing package database...")
  (package-refresh-contents)
  (dolist (pkg xianyyu/packages)
    (when (not (package-installed-p pkg))
      (package-install pkg))))




(scroll-bar-mode 0)
(setq cursor-type 'bar)
(setq inhibit-splash-screen t)
(setq initial-frame-alist (quote ((fullscreen . maximized))))

(setq initial-scratch-message "")

(defun header_snippet_socket ()
  "Format the given file as a source file."
  (interactive)
  (setq BaseFileName (file-name-sans-extension (file-name-nondirectory buffer-file-name)))
  (insert "#include <stdio.h>\n")
  (insert "#include <stdlib.h>\n")
  (insert "#include <string.h>\n")
  (insert "#include <sys/stat.h>\n")
  (insert "#include <sys/types.h>\n")
  (insert "#include <unistd.h>\n")
  (insert "#include <arpa/inet.h>")

  )


;(load-theme 'darktooth t)

(autoload 'qml-mode "qml-mode" "Editing Qt Declarative." t)
(add-to-list 'auto-mode-alist '("\\.qml$" . qml-mode))

(add-hook 'magit-status-mode-hook 'magit-filenotify-mode)

;;(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
;; 
;;(add-hook 'js-mode-hook 'js2-minor-mode)
;; 
;;(add-to-list 'interpreter-mode-alist '("node" . js2-mode))
;; 
;;(add-to-list 'auto-mode-alist '("\\.jsx?\\'" . js2-jsx-mode))
;;(add-to-list 'interpreter-mode-alist '("node" . js2-jsx-mode))



;;;;;;; google-c-style

(add-hook 'c-mode-common-hook 'google-set-c-style)
(add-hook 'c-mode-common-hook 'google-make-newline-indent)

(require 'popwin)
(popwin-mode t)

;;(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)
(global-set-key "\C-s" 'swiper)
;(global-set-key (kbd "C-c C-r") 'ivy-resume)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-c k") 'counsel-ag)

(global-set-key (kbd "C-c C-f") 'projectile-find-file-dwim)

;(blink-cursor-mode t)

(global-linum-mode t)
(global-company-mode t)
;(global-hungry-delete-mode t)


(global-undo-tree-mode)

(eval-after-load 'company
  '(progn
     (setq company-echo-delay 0)
     (setq company-idle-delay 0.2)
     (setq company-auto-complete nil)
     (setq company-show-numbers t)
     (setq company-begin-commands '(self-insert-command))
     (setq company-tooltip-limit 10)
     (setq company-minimum-prefix-length 3)
     ))

(add-to-list 'company-backends 'company-c-headers)


(global-set-key [f8] 'toggle-frame-fullscreen)

(global-set-key [f3] 'counsel-M-x)

;(global-set-key (kbd "C-c C-n") 'emms-seek-forward)
;(global-set-key (kbd "C-c C-b") 'emms-seek-backward)

(global-set-key [f10] 'emms-seek-forward)
(global-set-key [f9] 'emms-seek-backward)

(global-set-key [f11] 'emms-volume-lower)
(global-set-key [f12] 'emms-volume-raise)
(global-set-key (kbd "C-c C-s") 'emms-stop)


(global-set-key (kbd "C-c C-i") 'enlarge-window-horizontally)

(defun open-my-init-file()
  (interactive)
  (find-file "~/.emacs.d/init.el"))
(global-set-key [f1] 'open-my-init-file)
(global-set-key [f2] 'other-window)
;(global-set-key [f12] 'eval-buffer)
(global-set-key [escape] 'kill-buffer-and-window)


;; (add-to-list 'load-path
;;              "~/.emacs.d/personal/")


;;(require 'yasnippet)
;;(yas-global-mode 1)

(autoload 'lua-mode "lua-mode" "Lua editing mode." t)
    (add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
    (add-to-list 'interpreter-mode-alist '("lua" . lua-mode))

(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))


(require 'company-irony-c-headers)
(eval-after-load 'company
  '(add-to-list
    'company-backends '(company-irony-c-headers company-irony)))


(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
              (ggtags-mode 1))))


(require 'emms-setup)
(emms-standard)
(emms-default-players)


(require 'emms-player-mpv)
(add-to-list 'emms-player-list 'emms-player-mpv)


(autoload 'markdown-mode "markdown-mode"
  "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

(autoload 'gfm-mode "markdown-mode"
  "Major mode for editing GitHub Flavored Markdown files" t)
(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))

(global-set-key [f5] 'projectile-compile-project)
(global-set-key [f4] 'shell)

;;goto buffer's beginning and ending
(global-set-key [home] 'beginning-of-buffer)
(global-set-key [end] 'end-of-buffer)
;; don't backup files
(setq auto-save-mode nil)
(setq-default make-backup-files nil)
(setq make-backup-files nil)

;; For Linux
(global-set-key (kbd "<C-mouse-4>") 'text-scale-increase)
(global-set-key (kbd "<C-mouse-5>") 'text-scale-decrease)

;; (setq mode-line-format
;;       (list
;;        ;; value of `mode-name'
;;        "%m: "
;;        ;; value of current buffer name
;;        "buffer %b, "
;;        ;; value of current line number
;;        "line %l "
;;        "-- user: "
;;        ;; value of user
;;               (getenv "USER")))

(defun middle-of-line ()  
  "Put cursor at the middle point of the line."  
  (interactive)  
  (goto-char (/ (+ (point-at-bol) (point-at-eol)) 2)))  
(global-set-key (kbd "C-h") 'middle-of-line)

(setq auto-save-mode nil)
(setq-default make-backup-files nil)
(setq make-backup-files nil)

;(menu-bar-mode 0)
(tool-bar-mode 0)

;; For Linux
(global-set-key (kbd "<C-mouse-4>") 'text-scale-increase)
(global-set-key (kbd "<C-mouse-5>") 'text-scale-decrease)

(setq ring-bell-function 'ignore)
(delete-selection-mode t)
(setq visible-bell 0)
(fset 'yes-or-no-p 'y-or-n-p)
(setq auto-image-file-mode t)
(put 'scroll-left 'disabled nil)
(put 'scroll-right 'disabled nil)
(setq auto-save-default nil)
(put 'set-goal-column 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'LaTeX-hide-environment 'disabled nil)
(setq show-paren-style 'parenthesis)
(setq x-select-enable-clipboard t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#32302F" "#FB4934" "#B8BB26" "#FABD2F" "#83A598" "#D3869B" "#17CCD5" "#EBDBB2"])
 '(blink-cursor-mode nil)
 '(custom-enabled-themes nil)
 '(custom-safe-themes
   (quote
    ("596a1db81357b93bd1ae17bed428d0021d12f30cda7bbb31ac44e115039171ae" default)))
 '(pos-tip-background-color "#36473A")
 '(pos-tip-foreground-color "#FFFFC8")
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "unknown" :slant normal :weight normal :height 143 :width normal)))))
